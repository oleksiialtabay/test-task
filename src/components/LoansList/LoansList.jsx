import React, { useCallback, useState } from "react";
import classes from "./LoansList.module.css";
import Modal from "./Modal/Modal";

const LoansList = ({ data }) => {
  const [modal, setModal] = useState(false);
  const [state, setState] = useState();

  const openModal = useCallback((state) => {
    if (state.available !== "0") {
      setState(state);
      setModal(true);
    }
  }, []);

  const disabledButton = useCallback((available) => {
    if (available === "0") {
      return true;
    }
  }, []);

  const loans = Object.values(data.loans).reduce((acc, { available }) => {
    const numberAmount = parseFloat(available.replace(",", ""));
    return numberAmount + acc;
  }, 0);

  const clear = new Intl.NumberFormat().format(loans);

  const formattedTotalAmount = clear.replace(/\s/g, ",");

  return (
    <>
      {Object.values(data.loans).map((loan) => {
        return (
          <div key={loan.id}>
            <div onClick={() => openModal(loan)} className={classes.main}>
              <h1>{loan.title}</h1>
              <div className={classes.wrapper}>
                <div>
                  <p>Amount: &#163;{loan.amount}</p>
                  <p>Annualised return: {loan.annualised_return}</p>
                  <p>Amount available: &#163;{loan.available}</p>
                </div>
                <div>
                  {loan.available === "0" && (
                    <h3 className={classes.warn}>
                      You can't invest here anymore
                    </h3>
                  )}
                  {loan.invested && (
                    <h3 className={classes.success}>Invested</h3>
                  )}
                  <button
                    onClick={() => openModal(loan)}
                    disabled={disabledButton(loan.available)}
                    className={classes.btn}
                  >
                    invest
                  </button>
                </div>
              </div>
            </div>
            {modal && <Modal setModal={setModal} state={state} />}
          </div>
        );
      })}
      <p>Total amount available for investment: &#163;{formattedTotalAmount}</p>
    </>
  );
};

export default LoansList;
