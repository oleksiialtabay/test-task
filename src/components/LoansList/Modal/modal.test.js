import {handleInvest, msToTime} from "./helpers";


test("Should calculate values", () => {
    expect(handleInvest("3", "4")).toBe("1")
})

test("Should translate ms to normal Time", () => {
    expect(msToTime("864000")).toBe("00:14:24")
})
