import React, { useCallback, useState } from "react";
import classes from "./Modal.module.css";
import { handleInvest, msToTime } from "./helpers";

const Modal = ({ setModal, state }) => {
  const [invest, setInvest] = useState("");

  const onClose = useCallback(() => {
    setModal(false);
  }, []);

  const handleMainClick = ({ target }) => {
    if (target.dataset.close) {
      setModal(false);
    }
  };

  const disabledButton =
    Number(invest) > Number(state.available.replace(",", "")) || !invest && true;

  const hanldeInvest = useCallback(() => {
    state.available = handleInvest(invest, state.available);
    state.invested = true;
    setModal(false);
    return state;
  });

  return (
    <div onClick={handleMainClick} className={classes.main} data-close>
      <div className={classes.modal}>
        <button className={classes.closeButton} onClick={onClose}>
          &#10006;
        </button>
        <h1>Invest in Loan</h1>
        <p>{state.title}</p>
        <p>Amount available: &#163;{state.available}</p>
        <p>Loan ends in: {msToTime(state.term_remaining)}</p>
        <p>Investment amount (&#163;)</p>
        <div className={classes.wrapper}>
          <input
            autoFocus
            className={classes.input}
            value={invest}
            onChange={(e) => setInvest(e.target.value)}
            type="number"
          />
          <button
            className={classes.investButton}
            disabled={disabledButton}
            onClick={hanldeInvest}
          >
            Invest
          </button>
        </div>
      </div>
    </div>
  );
};

export default Modal;
