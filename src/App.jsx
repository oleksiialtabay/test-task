import classes from "./App.module.css";
import React from "react";
import LoansList from "./components/LoansList/LoansList";
import { data } from "./data";

const App = () => {
  return (
    <div className={classes.layout}>
      <h1>Current Loans</h1>
      <LoansList data={data} />
    </div>
  );
};

export default App;
